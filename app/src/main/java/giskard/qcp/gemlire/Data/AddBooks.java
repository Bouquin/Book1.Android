package giskard.qcp.gemlire.Data;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import giskard.qcp.gemlire.Book.Book;
import giskard.qcp.gemlire.Book.BookContent;

public class AddBooks {

    private SQLiteDB sql;
    private Context context;
    private static final int READ = 0;
    private static final int HAVE = 1;
    private static final int WISHLIST = 2;
    List<List<Book>> list = new ArrayList<List<Book>>();

    public AddBooks(Context context) {
        this.context = context;
        sql = new SQLiteDB(context);
    }

    public void read (Book book) { insertOrUpdateDataBase(READ, book); }

    public void have (Book book) {
        insertOrUpdateDataBase(HAVE, book);
    }

    public void wishlist (Book book) { insertOrUpdateDataBase(WISHLIST, book); }

    private void openDbIfNotOpen () {
        if (!sql.isOpen()) {
            sql.openDataBase();
        }
    }

    private void closeDbIfNotClosed () {
        if (sql.isOpen()) {
            sql.close();
        }
    }

    private void insertOrUpdateDataBase(int check, Book book) {
        boolean isWishlist = false;
        switch (check) {
            case READ:
                book.setRead(1);
                break;
            case HAVE:
                book.setHave(1);
                book.setWishlist(0);
                break;
            case WISHLIST:
                book.setWishlist(1);
                isWishlist = true;
                break;
        }
        BookContent.change = true;
        if (!bookExists(book)) {
            sql.insert(book.getContentValues());
            if (isWishlist) {
                BookContent.addBookWishlist(book);
            }
            else {
                BookContent.addBookHaveOrRead(book);
            }
        }
        else {
            sql.update(book.getContentValues());
                BookContent.modifyBook(book);
        }
        closeDbIfNotClosed();
    }

    public boolean bookExists (Book book) {
        openDbIfNotOpen();
        Cursor cursor = sql.selectValuesFrom("isbn", "books");
        if (cursor.moveToFirst()) {
            do {
                if (cursor.getString(0).equals(book.getIsbn())) {
                    return true;
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        return false;
    }

    public void loadAllBooksFromDb() {
        if (BookContent.firstTime) {
            openDbIfNotOpen();
            Cursor cursor = sql.selectAllFrom("books");
            if (cursor.moveToFirst()) {
                do {
                    byte[] thumbnail  = cursor.getBlob(3);
                    Bitmap bitmap = null;
                    if (thumbnail != null) {
                        ByteArrayInputStream imageStream = new ByteArrayInputStream(thumbnail);
                        bitmap = BitmapFactory.decodeStream(imageStream);
                    }
                    Book book = new Book(
                            cursor.getInt(0),
                            cursor.getString(1),
                            cursor.getString(2),
                            bitmap,
                            cursor.getString(4),
                            cursor.getString(5),
                            cursor.getString(6),
                            cursor.getInt(7),
                            cursor.getInt(8),
                            cursor.getInt(9),
                            cursor.getInt(10)
                    );
                    if (book.isWishlist() == 1) {
                        BookContent.addBookWishlist(book);
                    }
                    else {
                        BookContent.addBookHaveOrRead(book);
                    }

                } while (cursor.moveToNext());
            }
            cursor.close();
            closeDbIfNotClosed();
            BookContent.firstTime = false;
        }
    }
}

package giskard.qcp.gemlire.Data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.*;
import android.util.Log;
import android.widget.Toast;

import giskard.qcp.gemlire.Book.Book;

public class SQLiteDB extends SQLiteOpenHelper {
    private static String TAG = "DataBaseHelper";
    private SQLiteDatabase mDataBase;
    private static String DB_PATH = "";
    private static String DB_NAME ="books";
    private final Context mContext;

    public SQLiteDB(Context context) {
        super(context, DB_NAME, null, 1);// 1? its Database Version
        if(android.os.Build.VERSION.SDK_INT >= 17){
            DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        }
        else
        {
            DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        }
        this.mContext = context;
    }

    private boolean checkDataBase() {
        File dbFile = new File(DB_PATH + DB_NAME);
        return dbFile.exists();
    }

    //Copy the database from assets
    private void copyDataBase() throws IOException {
        InputStream mInput = mContext.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream mOutput = new FileOutputStream(outFileName);
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer))>0)
        {
            mOutput.write(mBuffer, 0, mLength);
        }
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }

    //Open the database, so we can query it
    public boolean openDataBase() throws SQLException {
        mDataBase = this.getWritableDatabase();
        return mDataBase != null;
    }

    public boolean isOpen () {
        if (mDataBase == null) {
            return false;
        }
        else {
            return mDataBase.isOpen();
        }
    }

    public boolean insert(ContentValues cV) {
        try {
            mDataBase.insertOrThrow("books", null, cV);
            close();
            return true;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(ContentValues cV) {
        try {
            mDataBase.update("books", cV, "isbn = " + cV.get("isbn"), null);
            close();
            return true;
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Cursor selectAllFrom(String table) {
        return selectValuesFrom("*", table);
    }
    public Cursor selectValuesFrom(String row, String table) {
       return readDataBase("select " + row +" from " + table);

    }

    private Cursor readDataBase (String query) {
        Cursor cursor  = mDataBase.rawQuery(query, null);
        return cursor;
    }

    @Override
    public synchronized void close() {
        if(mDataBase != null)
            mDataBase.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        mDataBase = db;
        mDataBase.execSQL("CREATE TABLE IF NOT EXISTS books" +
                        " ( id INTEGER PRIMARY KEY AUTOINCREMENT," +
                        " isbn TEXT NOT NULL," +
                        " title TEXT NOT NULL," +
                        " thumbnail BLOB," +
                        " date TEXT," +
                        " author TEXT," +
                        " publisher TEXT," +
                        " numberOfPages INT," +
                        " have BOOLEAN, " +
                        " read BOOLEAN," +
                        " wishlist BOOLEAN )"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

    }

}

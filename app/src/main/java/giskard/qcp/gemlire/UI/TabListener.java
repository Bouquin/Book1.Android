package giskard.qcp.gemlire.UI;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;

import java.util.ArrayList;
import java.util.List;

import giskard.qcp.gemlire.Book.Book;
import giskard.qcp.gemlire.Book.BookContent;

public class TabListener<T extends Fragment> implements ActionBar.TabListener {
    public Fragment mFragment;
    private final FragmentActivity mActivity;
    private final String mTag;

    public TabListener(FragmentActivity activity, String tag) {
        mActivity = activity;
        mTag = tag;
    }

    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        mFragment = mActivity.getSupportFragmentManager().findFragmentByTag(mTag);
        if (mFragment == null) {
            mFragment = BookFragment.newInstance(mTag);
            ft.add(android.R.id.content, mFragment, mTag);
        } else {
            ((BookFragment) mFragment).tag = mTag;
            ft.attach(mFragment);
        }

    }

    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        if (mFragment != null) {
            // Detach the fragment, because another one is being attached
            ft.detach(mFragment);
        }
    }

    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
        // User selected the already selected tab. Usually do nothing.
    }
}

package giskard.qcp.gemlire.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import giskard.qcp.gemlire.Book.Book;
import giskard.qcp.gemlire.Book.BookContent;
import giskard.qcp.gemlire.Data.AddBooks;
import giskard.qcp.gemlire.R;


public class MainActivity extends ActionBarActivity implements BookFragment.OnBookSelectedListener {

    public TabListener<Fragment> tabBook;
    public TabListener<Fragment> tabWishlist;
    private AddBooks addBook;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);

        addBook = new AddBooks(this);
        addBook.loadAllBooksFromDb();

        tabBook = new TabListener<Fragment>(this, "book");
        tabWishlist = new TabListener<Fragment>(this, "wishlist");

        ActionBar.Tab tab = actionBar.newTab().setText(R.string.book).setTabListener(tabBook);
        ActionBar.Tab tab1 = actionBar.newTab().setText(R.string.wishlist).setTabListener(tabWishlist);

        actionBar.addTab(tab);
        actionBar.addTab(tab1);

        if (savedInstanceState != null && savedInstanceState.getString("tab").equals(tab1.getText().toString())) {
            actionBar.selectTab(tab1);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("tab", actionBar.getSelectedTab().getText().toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        /*final MenuItem menuItem = menu.findItem(R.id.action_search);

        MenuItemCompat.setOnActionExpandListener(menuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) { return true; }
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) { return true; }
        });
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean queryTextFocused) {
                if(!queryTextFocused) {
                    searchView.onActionViewCollapsed();
                    searchView.setQuery("", false);
                }
            }
        });*/
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //case R.id.action_settings:
            //    break;
            case R.id.action_add:
                launchSearch();
                break;
            case R.id.action_scan:
                launchScanner();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void launchScanner() {
        Intent intent = new Intent(this, ScannerActivity.class);
        startActivityForResult(intent, 42);
    }

    private void launchSearch() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode == RESULT_OK) {
            String isbn = data.getStringExtra("result");
            showDetailBook(isbn, null);
        }
        else if (resultCode == RESULT_CANCELED) {
            Toast.makeText(this, "Camera unavailable", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBookSelected(View view, Book book) {
        showDetailBook(book.getIsbn(), book);
    }

    public void showDetailBook (String isbn, Book book) {
        Intent intent = new Intent(this, BookDetailActivity.class);
        if (!isbn.isEmpty()) {
            intent.putExtra("query", isbn);
            intent.putExtra("book", book);
        }
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        if (BookContent.change) {
            if (tabBook.mFragment != null) {
                ((BookFragment) tabBook.mFragment).loadBooks(BookContent.getBooksHaveOrRead());
            }
            if (tabWishlist.mFragment != null) {
                ((BookFragment) tabWishlist.mFragment).loadBooks(BookContent.getBooksWishlist());
            }
            BookContent.change = false;
        }
        super.onResume();
    }
}

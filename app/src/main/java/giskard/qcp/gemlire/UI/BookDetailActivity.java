package giskard.qcp.gemlire.UI;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.Loader;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import giskard.qcp.gemlire.Book.Book;
import giskard.qcp.gemlire.Data.AddBooks;
import giskard.qcp.gemlire.R;
import giskard.qcp.gemlire.Search.BookLoader;

public class BookDetailActivity extends FragmentActivity implements android.support.v4.app.LoaderManager.LoaderCallbacks<List<Book>>, View.OnClickListener {

    private Book book;
    private AddBooks addBook;
    private ProgressDialog progressDialog_;
    ImageView cover;
    TextView title;
    TextView isbn;
    TextView publisher;
    TextView date;
    TextView numberOfPages;
    ImageView bookRead;
    ImageView bookHave;
    ImageView bookWishlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_detail);

        cover = (ImageView) findViewById(R.id.cover);
        title = (TextView) findViewById(R.id.title);
        isbn = (TextView) findViewById(R.id.isbn);
        publisher = (TextView) findViewById(R.id.publisher);
        date = (TextView) findViewById(R.id.date);
        numberOfPages = (TextView) findViewById(R.id.numberofPages);
        bookRead = (ImageView) findViewById(R.id.bookRead);
        bookHave = (ImageView) findViewById(R.id.bookHave);
        bookWishlist = (ImageView) findViewById(R.id.bookWishlist);

        addListeners();
        addBook = new AddBooks(getApplicationContext());

        Bundle bundle = getIntent().getExtras();
        book = bundle.getParcelable("book");
        bundle.putString("context", "isbn");
        if (book != null) {
            fillLayout();
        }
        else {
            getSupportLoaderManager().restartLoader(0, bundle, this);
            launchProgressDialog();
        }
    }

    @Override
    public Loader<List<Book>> onCreateLoader(int id, Bundle args) {
        return (new BookLoader(this, args));
    }

    @Override
    public void onLoadFinished(Loader<List<Book>> loader, List<Book> books) {
        if (!books.isEmpty()) {
            book = books.get(0);
            fillLayout();
        }
        else {
            showError();
        }
        progressDialog_.dismiss();
    }

    private void fillLayout () {
        cover.setImageBitmap(book.getThumbnail());
        title.setText(book.getTitle());
        isbn.setText(String.valueOf(book.getIsbn()));
        publisher.setText(book.getPublisher());
        date.setText(book.getDate());
        numberOfPages.setText(String.valueOf(book.getNumberOfPages()));
        if (book.isRead() == 1) {
            bookRead.setImageResource(R.drawable.gem_jaune_pleine);
            bookRead.setClickable(false);
        }
        if (book.isHave() == 1) {
            bookHave.setImageResource(R.drawable.gem_bleue_pleine);
            bookHave.setClickable(false);
            bookWishlist.setClickable(false);
        }
        if (book.isWishlist() == 1) {
            bookWishlist.setImageResource(R.drawable.gem_rose_pleine);
            bookWishlist.setClickable(false);
        }
    }

    private void showError () {
        cover.setVisibility(View.INVISIBLE);
        findViewById(R.id.Title).setVisibility(View.INVISIBLE);
        title.setText("Pas de résultats");
        findViewById(R.id.Isbn).setVisibility(View.INVISIBLE);
        isbn.setVisibility(View.INVISIBLE);
        findViewById(R.id.Publisher).setVisibility(View.INVISIBLE);
        publisher.setVisibility(View.INVISIBLE);
        findViewById(R.id.Date).setVisibility(View.INVISIBLE);
        date.setVisibility(View.INVISIBLE);
        findViewById(R.id.NumberOfPages).setVisibility(View.INVISIBLE);
        numberOfPages.setVisibility(View.INVISIBLE);
        bookRead.setVisibility(View.INVISIBLE);
        findViewById(R.id.Read).setVisibility(View.INVISIBLE);
        bookHave.setVisibility(View.INVISIBLE);
        findViewById(R.id.Have).setVisibility(View.INVISIBLE);
        bookWishlist.setVisibility(View.INVISIBLE);
        findViewById(R.id.Wishlist).setVisibility(View.INVISIBLE);
    }

    private void addListeners() {
        bookRead.setOnClickListener(this);
        bookHave.setOnClickListener(this);
        bookWishlist.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bookRead:
                addBook.read(book);
                bookRead.setImageResource(R.drawable.gem_jaune_pleine);
                bookRead.setClickable(false);
            break;
            case R.id.bookHave:
                addBook.have(book);
                bookHave.setImageResource(R.drawable.gem_bleue_pleine);
                bookWishlist.setImageResource(R.drawable.gem_rose_vide);
                bookWishlist.setClickable(false);
                bookHave.setClickable(false);
            break;
            case R.id.bookWishlist:
                addBook.wishlist(book);
                bookWishlist.setImageResource(R.drawable.gem_rose_pleine);
                bookWishlist.setClickable(false);
                break;
        }
    }

    private void launchProgressDialog() {
        progressDialog_ = new ProgressDialog(this);
        progressDialog_.setMessage("Patientez...");
        progressDialog_.show();
    }

    @Override
    public void onLoaderReset(Loader<List<Book>> loader) {

    }
}
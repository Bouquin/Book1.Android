package giskard.qcp.gemlire.UI;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Window;

import me.dm7.barcodescanner.zbar.*;

public class ScannerActivity extends FragmentActivity implements ZBarScannerView.ResultHandler {
    private ZBarScannerView scannerView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        PackageManager pm = getApplicationContext().getPackageManager();

        if (!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            finish();
        }
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        try {
            scannerView = new ZBarScannerView(this);
            setContentView(scannerView);
        }
        catch (ExceptionInInitializerError e) {
            e.printStackTrace();
            finish();
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        scannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        scannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", rawResult.getContents());
        returnIntent.putExtra("type", rawResult.getBarcodeFormat().getName());
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        scannerView.stopCamera();
        finish();
    }
}

package giskard.qcp.gemlire.UI;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import giskard.qcp.gemlire.Book.Book;
import giskard.qcp.gemlire.Book.BookAdapter;
import giskard.qcp.gemlire.Book.BookContent;
import giskard.qcp.gemlire.R;
import giskard.qcp.gemlire.Search.SearchAdapter;

public class BookFragment extends Fragment implements AbsListView.OnItemClickListener {

    private OnBookSelectedListener mListener;
    private AbsListView mListView;
    private View view;
    public ArrayAdapter<Book> mAdapter;
    public String tag;
    private List<Book> listBooks = new ArrayList<Book>();
    private boolean isSearch = false;

    public static BookFragment newInstance(String tag) {
        BookFragment fragment = new BookFragment();
        Bundle args = new Bundle();
        args.putString("tag", tag);
        fragment.setArguments(args);
        return fragment;
    }

    public BookFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tag = getArguments().getString("tag");
        }
        else {
            tag = getTag();
        }

        if (mAdapter == null) {
            if (tag.equals("search")) {
                mAdapter = new SearchAdapter(getActivity().getApplicationContext(), new ArrayList<Book>());
            }
            else {
                mAdapter = new BookAdapter(getActivity().getApplicationContext(), new ArrayList<Book>());
            }
        }
        if (tag.equals("book")) {
            loadBooks(BookContent.getBooksHaveOrRead());
        }
        if (tag.equals("wishlist")) {
            loadBooks(BookContent.getBooksWishlist());
        }
        setRetainInstance(true);
    }

    public void loadBooks(List<Book> list) {
        mAdapter.clear();
        listBooks = list;
        Iterator<Book> it = listBooks.iterator();
        while (it.hasNext()) {
            mAdapter.add(it.next());
        }
        if (view != null) {
            if (mAdapter.isEmpty()) {
                view.findViewById(R.id.empty).setVisibility(View.VISIBLE);
            }
            else {
                view.findViewById(R.id.empty).setVisibility(View.INVISIBLE);
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_book, container, false);
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnBookSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListener) {
            mListener.onBookSelected(mAdapter.getView(position, null, parent), listBooks.get(position));
        }
    }

    public interface OnBookSelectedListener {
        public void onBookSelected(View view, Book book);
    }

}

package giskard.qcp.gemlire.UI;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import giskard.qcp.gemlire.Book.Book;
import giskard.qcp.gemlire.Book.BookContent;
import giskard.qcp.gemlire.R;
import giskard.qcp.gemlire.Search.BookLoader;

public class SearchActivity extends ActionBarActivity implements android.support.v4.app.LoaderManager.LoaderCallbacks<List<Book>>, BookFragment.OnBookSelectedListener {

    private ProgressDialog progressDialog;
    private BookFragment fragment;
    private Bundle bundle = new Bundle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);

        FragmentManager fM = getSupportFragmentManager();
        fragment = (BookFragment) fM.findFragmentById(R.id.fragment_search);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_search);

        MenuItemCompat.setOnActionExpandListener(menuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) { return true; }
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) { return true; }
        });
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setIconifiedByDefault(false);
        searchView.clearFocus();
        searchView.requestFocus();
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean queryTextFocused) {
                if (!queryTextFocused) {
                    searchView.onActionViewCollapsed();
                    searchView.setQuery("", false);
                }
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                launchProgressDialog();
                bundle.putString("query", s);
                bundle.putString("context", "search");
                getSupportLoaderManager().restartLoader(0, bundle, SearchActivity.this);
                searchView.onActionViewCollapsed();
                searchView.setQuery("", false);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBookSelected(View view, Book book) {
        showDetailBook(book.getIsbn(), null);
    }

    @Override
    public Loader<List<Book>> onCreateLoader(int id, Bundle args) {
        return (new BookLoader(this, args));
    }

    @Override
    public void onLoadFinished(Loader<List<Book>> loader, List<Book> books) {
        if (!books.isEmpty()) {
            ((TextView) fragment.getView().findViewById(R.id.empty)).setVisibility(View.INVISIBLE);
            BookContent.addAllBookSearch(books);
            fragment.loadBooks(BookContent.getBooksSearch());
        }
        else {
            //TODO: No result"
        }
        progressDialog.dismiss();
    }

    public void showDetailBook (String isbn, Book book) {
        Intent intent = new Intent(this, BookDetailActivity.class);
        intent.putExtra("query", isbn);
        intent.putExtra("book", book);
        startActivity(intent);
    }

    @Override
    public void onLoaderReset(Loader<List<Book>> loader) {

    }

    private void launchProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Patientez...");
        progressDialog.show();
    }
}

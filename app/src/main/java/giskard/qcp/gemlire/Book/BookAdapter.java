package giskard.qcp.gemlire.Book;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import giskard.qcp.gemlire.R;

public class BookAdapter extends ArrayAdapter<Book> {
    List<Book> list;
    private Context context;
    LayoutInflater inflater;
    private boolean useList = true;

    public BookAdapter(Context context, List<Book> items) {
        super(context, R.layout.item_book_list, items);
        inflater = LayoutInflater.from(context);
        this.context = context;
        list = items;
    }

    private class ViewHolder{
        TextView titleText;
        ImageView thumbnail;
        TextView author;
        ImageView read;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        Book book = getItem(position);
        View viewToUse = null;
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            if(useList){
                viewToUse = mInflater.inflate(R.layout.item_book_list, null);
            }
            else {
                viewToUse = mInflater.inflate(R.layout.item_book_grid, null);
            }
            holder = new ViewHolder();
            holder.titleText = (TextView)viewToUse.findViewById(R.id.bookName);
            holder.thumbnail = (ImageView)viewToUse.findViewById(R.id.bookThumbnail);
            holder.author = (TextView)viewToUse.findViewById(R.id.bookAuthor);
            holder.read = (ImageView) viewToUse.findViewById(R.id.bookRead);
            viewToUse.setTag(holder);
        }
        else {
            viewToUse = convertView; holder = (ViewHolder) viewToUse.getTag();
        }
        String title = book.getTitle();
        holder.titleText.setText(title);
        holder.thumbnail.setImageBitmap(book.getThumbnail());
        holder.author.setText(book.getAuthor());
        if (book.isRead() == 1) {
            holder.read.setImageResource(R.drawable.gem_jaune_pleine);
        }
        else {
            holder.read.setImageResource(R.drawable.gem_jaune_vide);
        }

        return viewToUse;
    }

}

package giskard.qcp.gemlire.Book;

import android.content.ContentValues;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.ByteArrayOutputStream;

public class Book implements Parcelable {
    private int id;
    private String isbn;
    private String title;
    private Bitmap thumbnail;
    private String date;
    private String author;
    private String publisher;
    private int numberOfPages;
    private int have = 0;
    private int read = 0;
    private int wishlist = 0;

    public Book() {}

    public Book(int id, String isbn, String title, Bitmap thumbnail, String date, String author, String publisher, int numberOfPages, int have, int read, int wishlist) {
        this.id = id;
        this.isbn = isbn;
        this.title = title;
        this.thumbnail = thumbnail;
        this.date = date;
        this.author = author;
        this.publisher = publisher;
        this.numberOfPages = numberOfPages;
        this.have = have;
        this.read = read;
        this.wishlist = wishlist;
    }

    public Book (Parcel in) {
        id = in.readInt();
        isbn = in.readString();
        title = in.readString();
        thumbnail = in.readParcelable(ClassLoader.getSystemClassLoader());
        date = in.readString();
        author = in.readString();
        publisher = in.readString();
        numberOfPages = in.readInt();
        have = in.readInt();
        read = in.readInt();
        wishlist = in.readInt();
    }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) { this.title = title; }

    public String getIsbn() {
        return isbn;
    }
    public void setIsbn(String isbn) { this.isbn = isbn; }

    public Bitmap getThumbnail() { return thumbnail; }
    public void setThumbnail(Bitmap thumbnail) { this.thumbnail = thumbnail; }

    public String getDate() { return date; }
    public void setDate(String date) { this.date = date; }

    public String getAuthor() { return author; }
    public void setAuthor(String author) { this.author = author; }

    public String getPublisher() { return publisher; }
    public void setPublisher(String publisher) { this.publisher = publisher; }

    public int getNumberOfPages() { return numberOfPages; }
    public void setNumberOfPages(int numberOfPages) { this.numberOfPages = numberOfPages; }

    public int isHave() { return have; }
    public void setHave(int have) { this.have = have; }

    public int isRead() { return read; }
    public void setRead(int read) { this.read = read; }

    public int isWishlist() { return wishlist; }
    public void setWishlist(int wishlist) { this.wishlist = wishlist; }

    public boolean isEmpty () {
        if (this.title.isEmpty()) {
            return true;
        }
        else {
            return false;
        }
    }

    public ContentValues getContentValues () {
        ContentValues cV = new ContentValues();
        cV.put("isbn", isbn);
        cV.put("title", title);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.PNG, 100, stream);
        cV.put("thumbnail", stream.toByteArray());
        cV.put("date", date);
        cV.put("author", author);
        cV.put("publisher", publisher);
        cV.put("numberOfPages", numberOfPages);
        cV.put("have", have);
        cV.put("read", read);
        cV.put("wishlist", wishlist);
        return cV;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(isbn);
        out.writeString(title);
        out.writeParcelable(thumbnail, flags);
        out.writeString(date);
        out.writeString(author);
        out.writeString(publisher);
        out.writeInt(numberOfPages);
        out.writeInt(have);
        out.writeInt(read);
        out.writeInt(wishlist);
    }

    public static final Parcelable.Creator<Book> CREATOR = new Parcelable.Creator<Book>() {
        @Override
        public Book createFromParcel(Parcel in) { return new Book(in); }
        @Override
        public Book[] newArray(int size) { return new Book[size]; }
    };
}

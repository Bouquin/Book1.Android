package giskard.qcp.gemlire.Book;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class BookContent {

    public static boolean change = false;
    public static boolean firstTime = true;

    private static List<Book> BooksHaveOrRead = new ArrayList<Book>();
    private static List<Book> BooksWishlist = new ArrayList<Book>();
    private static List<Book> BooksSearch = new ArrayList<Book>();

    public static List<Book> getBooksHaveOrRead() { return BooksHaveOrRead; }
    public static List<Book> getBooksWishlist() { return BooksWishlist; }
    public static List<Book> getBooksSearch() { return BooksSearch; }

    public static void addBookHaveOrRead(Book book) { BooksHaveOrRead.add(book); }
    public static void addBookWishlist(Book book) {
        BooksWishlist.add(book);
    }
    public static void addAllBookSearch(List<Book> books) {
        BooksSearch.clear();
        BooksSearch.addAll(books);
    }

    public static void modifyBook (Book book) {
            changeWishlist (book);
            changeBookOrRead(book);
    }

    private static void changeWishlist (Book book) {
        ListIterator<Book> it = BooksWishlist.listIterator();
        while (it.hasNext()) {
            if (it.next().getIsbn().equals(book.getIsbn())) {
                if (book.isWishlist() == 0) {
                    it.remove();
                }
                else {
                    it.set(book);
                }
            }
        }
    }

    private static void changeBookOrRead (Book book) {
        ListIterator<Book> it = BooksHaveOrRead.listIterator();
        while (it.hasNext()) {
            if (it.next().getIsbn().equals(book.getIsbn())) {
                if (book.isWishlist() == 1) {
                    it.remove();
                }
                else {
                    it.set(book);
                }
                return;
            }
        }
        addBookHaveOrRead(book);
    }
}
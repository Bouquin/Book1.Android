package giskard.qcp.gemlire.Search;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import giskard.qcp.gemlire.Book.Book;
import giskard.qcp.gemlire.R;

public class SearchByISBN {
    String request;
    String isbn;
    HttpClient client;
    HttpPost post;
    Context context;

    public SearchByISBN(String isbn, Context context) {
        this.context = context;
        this.isbn = isbn;
        // DO NOT FUCKING PUT A SLASH AT THE END OF THE REQUEST
        request = "http://www.openlibrary.org/api/books?bibkeys=ISBN:" + this.isbn + "&format=json&jscmd=data";
        client = new DefaultHttpClient();
        post = new HttpPost(request);
    }

    public List<Book> execute () {
        List<Book> listOfBooks = new ArrayList<Book>();
        try
        {
            HttpResponse response = client.execute(post);

            String jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
            JSONObject object = new JSONObject(jsonResult);
            String main = "ISBN:"+isbn;
            if (object.isNull(main)) {
                return listOfBooks;
            }

            Book book = new Book();
            object = new JSONObject(jsonResult).getJSONObject(main);
            if (object.getJSONObject("identifiers").has("isbn_13")) {
                book.setIsbn(object.getJSONObject("identifiers").getJSONArray("isbn_13").optString(0));
            }
            else {
                book.setIsbn(object.getJSONObject("identifiers").getJSONArray("isbn_10").optString(0));
            }
            if (object.has("title")) {
                book.setTitle(object.getString("title"));
            }
            if (object.has("cover")) {
                try {
                    InputStream is = (InputStream) new URL(object.getJSONObject("cover").getString("medium")).getContent();
                    Bitmap bitmap = BitmapFactory.decodeStream(is);
                    book.setThumbnail(bitmap);

                } catch (Exception e) {
                    e.printStackTrace();
                    book.setThumbnail(null);
                }
            }
            else {
                book.setThumbnail(BitmapFactory.decodeResource(context.getResources(), R.drawable.no_book));
            }
            if (object.has("publish_date")) {
                book.setDate(object.getString("publish_date"));
            }
            if (object.has("authors")) {
                book.setAuthor(((JSONObject) object.getJSONArray("authors").get(0)).getString("name"));
            }
            if (object.has("publishers")) {
                book.setPublisher(((JSONObject) object.getJSONArray("publishers").get(0)).getString("name"));
            }
            if (object.has("number_of_pages")) {
                book.setNumberOfPages(object.getInt("number_of_pages"));
            }

            listOfBooks.add(book);

        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        catch (ClientProtocolException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return listOfBooks;
    }

    private StringBuilder inputStreamToString(InputStream pInputString)
    {
        String rLine = "";
        StringBuilder answer = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(pInputString));

        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        }

        catch (IOException e) {
            e.printStackTrace();
        }
        return answer;
    }


}

package giskard.qcp.gemlire.Search;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;

import java.util.List;

import giskard.qcp.gemlire.Book.Book;

public class BookLoader extends AsyncTaskLoader<List<Book>> {

    private List<Book> bookList;
    private String query;
    private String whoAsks;
    private Context context;

    public BookLoader(Activity context, Bundle args) {
        super(context);
        this.context = context;
        query = args.getString("query");
        whoAsks = args.getString("context");
    }

    @Override
    public List<Book> loadInBackground() {
        if (whoAsks.equals("isbn")) {
            SearchByISBN rqt = new SearchByISBN(String.valueOf(query), context);
            bookList = rqt.execute();
        }
        else {
            SearchByKeyword rqt = new SearchByKeyword(String.valueOf(query));
            bookList = rqt.execute();
        }
        return bookList;
    }

    @Override
    public void deliverResult(List<Book> newBookList) {
        if (isReset()) {
            releaseResources(newBookList);
            return;
        }

        List<Book> oldBookList = bookList;
        oldBookList = newBookList;

        if (isStarted()) {
            super.deliverResult(newBookList);
        }
        if (oldBookList != null && oldBookList != newBookList) {
            releaseResources(oldBookList);
        }
    }

    @Override
    protected void onStartLoading() {
        if (bookList != null) {
            deliverResult(bookList);
        }
        if (takeContentChanged() || bookList == null) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    protected void onReset() {
        onStopLoading();
        if (bookList != null) {
            releaseResources(bookList);
            bookList = null;
        }
    }

    @Override
    public void onCanceled(List<Book> listBook) {
        super.onCanceled(listBook);
        releaseResources(listBook);
    }

    private void releaseResources(List<Book> listBook) {
    }
}

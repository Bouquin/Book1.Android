package giskard.qcp.gemlire.Search;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import giskard.qcp.gemlire.Book.Book;

public class SearchByKeyword {
    String request;
    HttpClient client;
    HttpPost post;

    public SearchByKeyword(String keyword) {
        keyword = keyword.replace(" ", "+");
        request = "http://www.openlibrary.org/search.json?q=" + keyword;
        client = new DefaultHttpClient();
        post = new HttpPost(request);
    }

    public List<Book> execute () {
        List<Book> listOfBooks = new ArrayList<Book>();
        try
        {
            HttpResponse response = client.execute(post);

            String jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
            JSONObject object = new JSONObject(jsonResult);
            if (object.isNull("docs")) {
                return listOfBooks;
            }

            JSONArray results = new JSONObject(jsonResult).getJSONArray("docs");

            int i = 0;
            while (i < results.length() || i < 50) {
                JSONObject jsonBook = (JSONObject) results.get(i++);
                Book book = new Book();

                if (jsonBook.has("isbn")) {
                    book.setIsbn(jsonBook.getJSONArray("isbn").optString(0));
                }
                String subtitle = "";
                if (jsonBook.has("subtitle")) {
                    subtitle  = ": " + jsonBook.getString("subtitle");
                }
                if (jsonBook.has("title")) {
                    book.setTitle(jsonBook.getString("title")+subtitle);
                }

                book.setThumbnail(null);

                if (jsonBook.has("first_publish_year")) {
                    book.setDate(jsonBook.getString("first_publish_year"));
                }
                if (jsonBook.has("author_name")) {
                    book.setAuthor(jsonBook.getJSONArray("author_name").optString(0));
                }
                if (jsonBook.has("publishers")) {
                    book.setPublisher(jsonBook.getJSONArray("publisher").optString(0));
                }

                book.setNumberOfPages(0);

                listOfBooks.add(book);
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        catch (ClientProtocolException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return listOfBooks;
    }

    private StringBuilder inputStreamToString(InputStream pInputString)
    {
        String rLine = "";
        StringBuilder answer = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(pInputString));

        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        }

        catch (IOException e) {
            e.printStackTrace();
        }
        return answer;
    }
}

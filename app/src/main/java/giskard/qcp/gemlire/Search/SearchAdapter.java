package giskard.qcp.gemlire.Search;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import giskard.qcp.gemlire.Book.Book;
import giskard.qcp.gemlire.R;

public class SearchAdapter extends ArrayAdapter<Book> {
    List<Book> list;
    private Context context;
    LayoutInflater inflater;
    private boolean useList = true;

    public SearchAdapter(Context context, List<Book> items) {
        super(context, R.layout.item_search_list, items);
        inflater = LayoutInflater.from(context);
        this.context = context;
        list = items;
    }

    private class ViewHolder{
        TextView titleText;
        TextView author;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        Book book = getItem(position);
        View viewToUse = null;
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            if(useList){
                viewToUse = mInflater.inflate(R.layout.item_search_list, null);
            }
            else {
                viewToUse = mInflater.inflate(R.layout.item_search_grid, null);
            }
            holder = new ViewHolder();
            holder.titleText = (TextView)viewToUse.findViewById(R.id.bookName);
            holder.author = (TextView)viewToUse.findViewById(R.id.bookAuthor);
            viewToUse.setTag(holder);
        }
        else {
            viewToUse = convertView; holder = (ViewHolder) viewToUse.getTag();
        }
        holder.titleText.setText(book.getTitle());
        holder.author.setText(book.getAuthor());

        return viewToUse;
    }

}
